Mettre en ligne de la doc avec un dépôt git.
Auteur : C. Poulard, 2022


**Plan**

Le wiki développe les points suivants:

[TOC]

# Atelier Gitlab Flavored Markdown 

## Objectif 
l'objectif est de regrouper ici toutes les infos pour rédiger ce **readme.md** et toute la partie [wiki](https://gitlab.irstea.fr/christine.poulard/atelier-gitlab-flavored-mardown/-/wikis/home), surtout celles qui sont difficiles à trouver (attention, les fonctions évoluent ! ! )

## les fonctions de base
Les fonctions courantes sont faciles à comprendre, et proposées pour la plupart dans la barre d'outil : niveaux de titre (header), **gras**, _italique_, liste d'items, etc..

## mise en forme "niveau intermédiaire"
D'autres fonctionnalités méritent d'être connues ; la syntaxe est parfois légèrement différente du "Markdown classique":
1. mettre du texte en évidence :  citations, exemple d'`instruction de code`, <kbd>effet clavier</kbd>, code avec coloration syntaxique, mettre du texte en couleur - dont une [méthode astucieuse pour du bleu]("oui, c'est un lien sans URL").

1. organiser son texte :  
- "section déroulante" (collapsible section) 
    
<details><summary>si vous ne voyez pas ce qu'est une "collapsible section", cliquez ici</summary>
Ici, on vous en dit plus ; pour la **mise en forme** et les `citations de code` voir le <kbd>wiki</kbd>.
</details>

- découpage en pages et en sections avec création de **liens** (externes, vers d'autres pages [du wiki par exemple home](https://gitlab.irstea.fr/christine.poulard/atelier-gitlab-flavored-mardown/-/wikis/home) 
- **tables des matières dans une page** (Table of Content) et **table des pages** (sidebar)
- insérer une image, un tableau (dans les 2 cas: modalités différentes selon l'éditeur choisi), une équation 
- pourquoi pas : les emoji :bulb: 

## autres outils
- les graphes mermaid (voir exemple en bas de ce readme)

## tirer parti de la nature de  "dépôt git" du wiki
Le wiki est lui-même un **dépôt git" (repository), comme le sont les codes dans la partie "Repository". Il est donc possible de le **cloner**, en récupérant les infos via une icône en haut à droite. Une fois que l'on a rappatrié les fichiers markdown et le répertoire Images, on peut les modifier en local (sous Windows : éditer le mardown par exemple avec PyCharm, VScode...), puis et 'commiter' et 'pusher' pour mettre à jour le wiki.
A noter que, de la même manière que gitlab permet de régler les droits sur le dépôt du projet, il permet de régler également les droits de lecture et de modification du wiki, indépendamment de ceux du projet.

## (A venir) Sphinx, pour une doc et/ou une description des classes extraite automatiquement

à venir


## Annexe : proposition de graphique mermaid pour décrire le wiki... version 0.01
```mermaid
graph TB
  subgraph Pages
    subgraph Page1
       S1[Section 11]:::blue;
       S2[Section 12]:::blue;
       S2-.->|hyperlien| S3;
       S3[Section 13 <br> hyper importante]:::blue;
    end
    subgraph Page2
       S4[Section 21]:::Ao_green;
       S5[Section 22]:::Ao_green;
       S5-.->|hyperlien| S3;
       S6[Section 23]:::Ao_green;
       S6-.->|hyperlien| S4;
    end
    
  end 
  subgraph SideBar
    direction TB
    subgraph Section1
      B1[Page 1]:::brass ;
      B13[Page 1 section3]:::brass -.-> S3
    end
    subgraph Section2
      B2[Page 2]:::brass;
      
    end
  end 

classDef blue fill:#58a;
classDef brass fill:#c97;
classDef Ao_green fill:#080;
classDef gold fill:#fa0;
```




